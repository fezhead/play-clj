(defproject {{app-name}} "0.0.1-SNAPSHOT"
  :description "FIXME: write description"

  :dependencies [[com.badlogicgames.gdx/gdx "1.11.0"]
                 [com.badlogicgames.gdx/gdx-backend-lwjgl3 "1.11.0"]
                 [com.badlogicgames.gdx/gdx-box2d "1.11.0"]
                 [com.badlogicgames.gdx/gdx-box2d-platform "1.11.0"
                  :classifier "natives-desktop"]
                 [com.badlogicgames.gdx/gdx-bullet "1.11.0"]
                 [com.badlogicgames.gdx/gdx-bullet-platform "1.11.0"
                  :classifier "natives-desktop"]
                 [com.badlogicgames.gdx/gdx-platform "1.11.0"
                  :classifier "natives-desktop"]
                 [org.clojure/clojure "1.11.1"]
                 [play-clj "1.2.0"]]

  :source-paths ["src" "src-common"]
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :aot [{{desktop-namespace}}]
  :main {{desktop-namespace}})
